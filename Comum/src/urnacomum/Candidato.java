/*
 * Nomes(NºUSP) :
 * 
 * Fabrício Pereira de Freitas (8910372)
 * Pedro Carrascosa C. Dias (7173480)
 * Ricardo Chagas (8957242)
 */
package urnacomum;

import java.io.Serializable;

/**
 *
 * @author ricardo
 */
public class Candidato implements Serializable {
    private final int codigo_votacao;
    private final String nome_candidato;
    private final String partido;
    private int num_votos;

    public Candidato(int codigo_votacao, String nome_candidato, String partido) {
        this.codigo_votacao = codigo_votacao;
        this.nome_candidato = nome_candidato;
        this.partido = partido;
        this.num_votos = 0;
    }
    
    public int getCodigo_votacao() {
        return codigo_votacao;
    }

    public String getNome_candidato() {
        return nome_candidato;
    }

    public String getPartido() {
        return partido;
    }

    public int getNum_votos() {
        return num_votos;
    }

    public void setNum_votos(int num_votos) {
        this.num_votos = num_votos;
    }
    
    @Override
    public String toString() {
        return ("Nome: " + this.nome_candidato + "\nPartido: " + this.partido + "\nNumero de votos: " + this.num_votos);
    }
}

