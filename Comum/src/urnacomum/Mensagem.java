/*
 * Nomes(NºUSP) :
 * 
 * Fabrício Pereira de Freitas (8910372)
 * Pedro Carrascosa C. Dias (7173480)
 * Ricardo Chagas (8957242)
 */
package urnacomum;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author ricardo
 */
public class Mensagem implements Serializable {
    private final int opcode;
    private final ArrayList<Candidato> dados;
    private int brancos;
    private int nulos;    
    
    public Mensagem(int opcode, ArrayList<Candidato> dados) {
        this.opcode = opcode;
        this.dados = new ArrayList();
        this.dados.addAll(dados);
    }
    
    public void setBrancos(int brancos) {
        this.brancos = brancos;
    }

    public void setNulos(int nulos) {
        this.nulos = nulos;
    }

    public int getBrancos() {
        return brancos;
    }

    public int getNulos() {
        return nulos;
    }

    public int getOpcode() {
        return opcode;
    }

    public ArrayList<Candidato> getDados() {
        return dados;
    }
}
